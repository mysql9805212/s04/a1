-- Let's add a few more entries.
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 181, "Rock and roll", 15);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 15);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 16);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 192, "Dancehall-poptropical housemoombahton", 11);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 12);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 19);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019", 8);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 196, "Pop, R&B", 20);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016", 9);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, disco, R&B", 21);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011", 9);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 192, "Pop", 22);

-- You can export a database from XAMPP using the Export Tab.
-- Import data by selecting the database you'd like the data put into then go to the Import Tab.

-- [Section] Advanced Selection Queries
-- Counting number of items
SELECT COUNT(genre) FROM songs WHERE genre = "Pop"; -- Returns count of items meeting given condition

-- Exclude records or NOT operator
SELECT * FROM songs WHERE id != 3;

-- Greater Than or Equal;
SELECT * FROM songs WHERE id > 3;

SELECT * FROM songs WHERE id >= 3;

-- Less Than or Equal;
SELECT * FROM songs WHERE id < 11;

SELECT * FROM songs WHERE id <= 11;

-- IN operator to return specific results
SELECT * FROM songs WHERE id IN (1, 3, 11); -- Will retrieve songs that have only these given ids

SELECT * FROM songs WHERE genre IN ("Pop", "K-Pop");

-- Partial Matches
SELECT * FROM songs WHERE song_name LIKE "%a"; -- Returns songs that have "a" at the end of its title

SELECT * FROM songs WHERE song_name LIKE "a%"; -- Returns songs starting with "a"

SELECT * FROM songs WHERE song_name LIKE "%a%"; -- Returns songs that have "a" in its title, regardless of the position

SELECT * FROM songs WHERE song_name LIKE "__s_y_"; -- Returns the song "Masaya" so long as the underscores are correct

SELECT * FROM songs WHERE song_name LIKE "%say%";

SELECT * FROM songs WHERE song_name LIKE "%s%y%";

SELECT * FROM songs WHERE song_name LIKE "%s_y%";

SELECT * FROM songs WHERE id LIKE "%1";

-- SORTING records
SELECT * FROM songs ORDER BY song_name ASC; -- Returns songs in ascending alphabetical order

SELECT * FROM songs ORDER BY song_name DESC; -- Returns songs in descending alphabetical order

-- Getting distinct records
SELECT DISTINCT genre FROM songs; -- Shows the genres currently available in the table (SELECT genre FROM songs returns the genre of EACH row in the table)

-- [Section] Joining Tables
-- INNER JOIN or JOIN: Merges rows from two tables that have matching info; leaves out rows or items that don't have info matching with other table (ex. albums without any songs won't be included in the returned list and vice versa); uses the Foreign Key as reference
-- Returns the "intersection" of two tables, using a foreign key as the reference

SELECT * FROM albums JOIN songs ON albums.id = songs.album_id;

-- SELECT all FROM albums to JOIN with songs based ON albums.id, which is songs.album_id (the foreign key in songs)

SELECT albums.album_title, songs.song_name FROM 
	albums JOIN songs ON albums.id = songs.album_id;

-- albums is the "left" table, while songs is the "right" one that will be merged into albums
-- Use dot notation to inform the program where the specified columns can be found

-- Joining multiple tables via Inner Join
-- artists > album > songs
SELECT artists.name, albums.album_title, songs.song_name FROM artists
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

-- LEFT JOIN: includes all items from the "left" table even if it doesn't have a corresponding entry in the "right" table. The "left" row w/o a corresponding entry from the "right" will simply be lined up with a NULL result in the returned table.

SELECT albums.album_title, songs.song_name FROM albums LEFT JOIN songs ON albums.id = songs.album_id;

-- RIGHT JOIN: includes all items from the "right" table even if it has no corresponding entry in the "left" table
SELECT * FROM albums RIGHT JOIN songs ON albums.id = songs.album_id; -- In this case, we won't see a NULL result based on our database's structure (can't enter songs without a corresponding album).

-- OUTER JOIN: joins all tables even if there are no corresponding entries from either the "left" or "right" tables
SELECT * FROM albums LEFT OUTER JOIN songs ON albums.id = songs.album_id
	UNION
	SELECT * FROM albums RIGHT OUTER JOIN songs ON albums.id = songs.album_id;
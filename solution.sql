-- Artists with D in name
SELECT * FROM artists WHERE name LIKE "%d%";

-- All songs with length of less than 230
SELECT * FROM songs WHERE length < 230;

-- Join "albums" and "songs" tables, showing only album name, song name, and song length
SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;

-- Join "artists" and "albums" tables + find all albums with A in its name
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- Sort albums in Z-A order, showing only the first 4 records
SELECT * FROM albums WHERE id <= 4 ORDER BY album_title DESC;

-- Join "albums" and "songs" tables, sorting the albums from Z-A
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC;